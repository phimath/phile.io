namespace phimath.phileio.Logging
{
    public enum PeriodicityOptions {
        DAILY,
        HOURLY,
        MINUTELY,
        MONTHLY
    }
}