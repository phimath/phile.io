using System;
using System.IO;
using System.Net;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using phimath.phileio.Logging;

namespace phimath.phileio.Upload
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(l => l.AddFile(o =>
            {
                o.Extension = ".log";
                o.FileName = "phileio.upload.";
                o.LogDirectory = Path.GetFullPath(Configuration.GetValue<string>("Logging:Directory"));
            }));

            services.Configure<KestrelServerOptions>(Configuration.GetSection("Kestrel"));

            services.Configure<ForwardedHeadersOptions>(o =>
            {
                o.ForwardLimit = null;
                o.AllowedHosts.Add("*");
                o.KnownNetworks.Add(new IPNetwork(IPAddress.Parse("172.16.8.0"), 24)); // Custom
                o.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedHost;
            });

            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            var basePath = Configuration.GetValue<string>("BasePath");
            if (basePath != null)
            {
                app.UsePathBase(basePath);
                Console.WriteLine("Using base path: " + basePath);
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapControllers();
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}