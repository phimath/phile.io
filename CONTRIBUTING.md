If you want to contribute, 

*  send me an email to [philipp@phimath.de](mailto:philipp@phimath.de),
*  or contact me on Twitter [@p_matthaeus](https://twitter.com/p_matthaeus).