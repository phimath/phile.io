using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using phimath.phileio.Data;
using phimath.phileio.Models;

namespace phimath.phileio.Controllers
{
    [Route("/logon")]
    public class LogonController : Controller
    {
        private RepositoryContext Db { get; }

        public LogonController(RepositoryContext db)
        {
            Db = db;
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> Index(string returnUrl = null)
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
            {
                await HttpContext.ChallengeAsync(new OAuthChallengeProperties
                {
                    RedirectUri = returnUrl ?? "/"
                });
            }

            Db.EnsureUserCreated();

            return Redirect(returnUrl ?? "/");
        }
    }
}