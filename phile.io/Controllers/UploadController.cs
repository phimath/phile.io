using System;
using System.IO;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Newtonsoft.Json;
using phimath.phileio.Services;

namespace phimath.phileio.Controllers
{
    public class UploadController : ControllerBase
    {
        private struct ChunkMetadata
        {
            public int Index { get; set; }
            public int TotalCount { get; set; }
            public int FileSize { get; set; }
            public string FileName { get; set; }
            public string FileType { get; set; }
            public string FileGuid { get; set; }
            public IRepositoryDirectory Target { get; set; }
        }

        private readonly IRepositoryAccessor _fs;


        public UploadController(IRepositoryAccessor fs)
        {
            _fs = fs;
        }

        [HttpPost]
        [Route("repository/{id:int}/upload/{**folder}")]
        public ActionResult IndexAction(IFormFile myFile, string chunkMetadata, int id, string folder)
        {
            var tempPath = Path.Combine(Path.GetTempPath(), "uploads");
            // Removes temporary files
            RemoveTempFilesAfterDelay(tempPath, new TimeSpan(0, 5, 0));

            try
            {
                if (!string.IsNullOrEmpty(chunkMetadata))
                {
                    var metaDataObject = JsonConvert.DeserializeObject<ChunkMetadata>(chunkMetadata);

                    var tempFilePath = Path.Combine(tempPath, metaDataObject.FileGuid + ".tmp");
                    if (!Directory.Exists(tempPath))
                    {
                        Directory.CreateDirectory(tempPath);
                    }

                    AppendChunkToFile(tempFilePath, myFile);

                    if (metaDataObject.Index == (metaDataObject.TotalCount - 1))
                    {
                        metaDataObject.Target = (IRepositoryDirectory) _fs.Root(id).GetRelativePath(folder);
                        SaveUploadedFile(tempFilePath, metaDataObject);
                    }
                }
            }
            catch
            {
                return BadRequest();
            }

            return Ok();
        }

        void AppendChunkToFile(string path, IFormFile content)
        {
            using (var stream = new FileStream(path, FileMode.Append, FileAccess.Write))
            {
                content.CopyTo(stream);
            }
        }

        void SaveUploadedFile(string tempFilePath, ChunkMetadata meta)
        {
            var fileName = meta.FileName;
            if (meta.Target.Files.Any(f => f.Name == fileName))
            {
                var append = 1;
                while (meta.Target.Files.Any(f => f.Name == fileName + $"-{append}"))
                {
                    ++append;
                }

                fileName += $"-{append}";
            }

            var file = meta.Target.CreateFile(fileName);
            file.SetContent(System.IO.File.ReadAllBytes(tempFilePath));
        }

        void RemoveTempFilesAfterDelay(string path, TimeSpan delay)
        {
            var dir = new DirectoryInfo(path);
            if (dir.Exists)
            {
                foreach (var file in dir.GetFiles("*.tmp").Where(f => f.LastWriteTimeUtc.Add(delay) < DateTime.UtcNow))
                {
                    file.Delete();
                }
            }
        }
    }
}