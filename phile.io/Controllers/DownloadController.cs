using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using phimath.phileio.Data;
using phimath.phileio.Services;
using MediaTypeHeaderValue = System.Net.Http.Headers.MediaTypeHeaderValue;

namespace phimath.phileio.Controllers
{
    [Controller]
    public class DownloadController : Controller
    {
        private readonly RepositoryContext _db;
        private readonly IRepositoryAccessor _fs;

        public DownloadController(RepositoryContext db, IRepositoryAccessor fs)
        {
            _db = db;
            _fs = fs;
        }


        [Authorize]
        [HttpGet]
        [Route("/repository/{id}/-/{**path}")]
        [ActionName("Download")]
        public IActionResult DownloadAction(int id, string path)
        {
            Repository repo;
            try
            {
                repo = _db.GetRepositoryAndCheckAccess(id);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }
            catch (UnauthorizedAccessException)
            {
                return Unauthorized();
            }

            var data = _fs.DownloadPath(id, $"/{path}", out var fileName);
            return File(data, "application/octet-stream", fileName);
        }

        [HttpGet]
        [Route("/share/{shareGuid}")]
        public IActionResult Share(Guid shareGuid)
        {
            var share = _db.Find<Share>(shareGuid);
            if (share == null)
            {
                return NotFound();
            }

            var data = _fs.DownloadPath(share.RepositoryId, share.RelativePath, out var fileName);
            return File(data, "application/octet-stream", fileName);
        }
    }
}