using System;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.WsFederation;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using phimath.phileio.Data;
using phimath.phileio.Pages.Repository;
using phimath.phileio.Services;

namespace phimath.phileio
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<RepositoryContext>(builder => { builder.UseSqlite(Configuration.GetConnectionString("DefaultConnection")); });
            services.AddSingleton<IRepositoryAccessor>(sp => new FsRepositoryAccessor(Configuration["RepositoryRoot"], Convert.ToBoolean(Configuration["RepositoryIsLazy"])));
            services
                .AddAuthentication(sharedOptions =>
                {
                    sharedOptions.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    sharedOptions.DefaultChallengeScheme = WsFederationDefaults.AuthenticationScheme;
                })
                .AddWsFederation(options =>
                {
                    options.Wtrealm = Configuration["wsfed:realm"];
                    options.MetadataAddress = Configuration["wsfed:metadata"];
                })
                .AddCookie();
            services.AddHttpContextAccessor();
            services.AddScoped<IUserAccessor>(sp => new ClaimsPrincipalUserAccessor(sp.GetService<IHttpContextAccessor>()));
            services.AddMvc();
            services.AddRazorPages();
            services.AddServerSideBlazor();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts(); // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapControllers();
                endpoints.MapRazorPages();
                endpoints.MapFallbackToController("/repository/{id}/-/{**path}", "Download", "Download");
                endpoints.MapFallbackToPage("/_Host");
            });
        }
    }
}