using System.Collections.Generic;

namespace phimath.phileio.Models
{
    public class AddRepositoryModel
    {
        public string Name { get; set; }
        public string Owner { get; set; }
        
        public IEnumerable<string> AdditionalManagers { get; set; } = new List<string>();
        public IEnumerable<string> AdditionalMembers { get; set; } = new List<string>();
    }
}