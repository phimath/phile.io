using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace phimath.phileio.Services
{
    public interface IRepositoryAccessor
    {
        IRepositoryDirectory CreateRepository(int repositoryId);
        IRepositoryDirectory Root(int repositoryId);

        public byte[] DownloadPath(int repositoryId, string relativePath, out string downloadFileName)
        {
            var result = Root(repositoryId).GetRelativePath(relativePath);
            switch (result)
            {
                case IRepositoryDirectory dir:
                    downloadFileName = dir.Name + ".zip";
                    return dir.ToZip();
                case IRepositoryFile file:
                    downloadFileName = file.Name;
                    return file.GetContent();
                default: throw new ArgumentOutOfRangeException(nameof(result));
            }
        }
    }

    public interface IRepositoryNode
    {
        string Name { get; set; }

        string Path { get; }

        IRepositoryDirectory Parent { get; }

        DateTime CreatedOn { get; }
    }

    public interface IRepositoryDirectory : IRepositoryNode
    {
        IEnumerable<IRepositoryNode> Children { get; }
        IEnumerable<IRepositoryDirectory> Directories { get; }
        IEnumerable<IRepositoryFile> Files { get; }
        IRepositoryDirectory CreateDirectory(string name);
        IRepositoryFile CreateFile(string name);
        void DeleteChild(IRepositoryNode node);
        IRepositoryNode GetRelativePath(string relativePath);

        public byte[] ToZip()
        {
            var zipStream = new MemoryStream();
            var zip = new ZipArchive(zipStream, ZipArchiveMode.Create);
            AddToZip(ref zip);
            return zipStream.ToArray();
        }

        protected void AddToZip(ref ZipArchive archive)
        {
            foreach (var file in Files)
            {
                var entry = archive.CreateEntry(file.Path, CompressionLevel.Fastest);
                var entryStream = entry.Open();
                entryStream.Write(new ReadOnlySpan<byte>(file.GetContent()));
                entryStream.Close();
            }

            foreach (var directory in Directories)
            {
                directory.AddToZip(ref archive);
            }
        }
    }

    public interface IRepositoryFile : IRepositoryNode
    {
        void SetContent(byte[] value);
        byte[] GetContent();
        public long Size { get; }
    }
}