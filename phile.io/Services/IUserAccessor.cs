using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace phimath.phileio.Services
{
    public interface IUserAccessor
    {
        public string NameId { get; }
        public string Name { get; }
        public string GivenName { get; }
        public string Surname { get; }
        public bool IsAuthenticated { get; }
    }

    public class ClaimsPrincipalUserAccessor : IUserAccessor
    {
        public string NameId => _accessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value;
        public string Name => _accessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
        public string GivenName => _accessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.GivenName)?.Value;
        public string Surname => _accessor?.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == ClaimTypes.Surname)?.Value;
        public bool IsAuthenticated => _accessor?.HttpContext?.User?.Identity?.IsAuthenticated ?? false;

        private readonly IHttpContextAccessor _accessor;

        public ClaimsPrincipalUserAccessor(IHttpContextAccessor accessor)
        {
            _accessor = accessor ?? throw new ArgumentNullException(nameof(accessor));
        }
    }
}