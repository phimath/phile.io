using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace phimath.phileio.Services
{
    public class FsRepositoryAccessor : IRepositoryAccessor
    {
        private readonly bool _isLazy;
        private readonly string _root;

        public FsRepositoryAccessor(string root, bool isLazy = true)
        {
            _isLazy = isLazy;
            _root = Path.GetFullPath(root);
            if (!Directory.Exists(_root))
            {
                throw new ArgumentException("File root must be an existing directory!", nameof(root));
            }
        }

        public IRepositoryDirectory CreateRepository(int repositoryId)
        {
            Directory.CreateDirectory(Path.Join(_root, repositoryId.ToString()));
            return Root(repositoryId);
        }

        public IRepositoryDirectory Root(int repositoryId)
        {
            return new FsLazyRepositoryRoot(new DirectoryInfo(Path.Join(_root, repositoryId.ToString())));
        }
    }

    public class FsLazyRepositoryDirectory : IRepositoryDirectory
    {
        private readonly DirectoryInfo _di;
        private readonly List<IRepositoryNode> _children;

        public FsLazyRepositoryDirectory(DirectoryInfo di, IRepositoryDirectory parent)
        {
            Parent = parent;
            _di = di;
            _children = _di.EnumerateDirectories().Select(di2 => (IRepositoryNode) new FsLazyRepositoryDirectory(di2, this)).Concat(
                _di.EnumerateFiles().Select(fi => (IRepositoryNode) new FsLazyRepositoryFile(fi, this))).ToList();
            SetRelativePath();
        }

        public virtual string Name
        {
            get => _di.Name;
            set
            {
                ValidateName(value);
                _di.MoveTo(System.IO.Path.Join(_di.Parent.FullName, value));
            }
        }

        public virtual string Path { get; private set; }
        public virtual IRepositoryDirectory Parent { get; private set; }
        public DateTime CreatedOn => _di.CreationTime;

        public IEnumerable<IRepositoryNode> Children => _children;
        public IEnumerable<IRepositoryDirectory> Directories => _children.OfType<IRepositoryDirectory>();
        public IEnumerable<IRepositoryFile> Files => _children.OfType<IRepositoryFile>();

        private void SetRelativePath()
        {
            if (Parent != null)
            {
                Path = Parent.Path + Name + "/";
            }
        }

        private static void ValidateName(string name)
        {
            if (name.Contains(System.IO.Path.DirectorySeparatorChar) ||
                name.Contains(System.IO.Path.AltDirectorySeparatorChar) ||
                name.Contains(System.IO.Path.VolumeSeparatorChar))
            {
                throw new ArgumentException("Name may not contain path separators", nameof(name));
            }
        }

        internal virtual void DeleteInternal()
        {
            _di.Delete(true);
        }

        public IRepositoryDirectory CreateDirectory(string name)
        {
            var dir = new FsLazyRepositoryDirectory(_di.CreateSubdirectory(name), this);
            _children.Add(dir);
            return dir;
        }

        public IRepositoryFile CreateFile(string name)
        {
            var fi = new FileInfo(System.IO.Path.Join(_di.FullName, name));
            fi.Create().Close();
            var dir = new FsLazyRepositoryFile(fi, this);
            _children.Add(dir);
            return dir;
        }

        public void DeleteChild(IRepositoryNode node)
        {
            switch (node)
            {
                case FsLazyRepositoryDirectory dir:
                    dir.DeleteInternal();
                    break;
                case FsLazyRepositoryFile file:
                    file.DeleteInternal();
                    break;
                default: throw new NotSupportedException();
            }
        }

        public virtual IRepositoryNode GetRelativePath(string relativePath)
        {
            if (relativePath == null)
            {
                throw new ArgumentNullException(nameof(relativePath));
            }

            if (relativePath == string.Empty)
            {
                return this;
            }

            if (relativePath.StartsWith("/"))
            {
                IRepositoryDirectory root = this;
                do
                {
                    root = root.Parent;
                } while (!(root is FsLazyRepositoryRoot));

                return root.GetRelativePath(relativePath);
            }

            var idx = relativePath.IndexOf("/", StringComparison.Ordinal);
            if (idx < 0)
            {
                // Try file
                return Files.FirstOrDefault(f => f.Name == relativePath) ?? throw new FileNotFoundException();
            }

            var nextName = relativePath.Substring(0, idx);
            return Directories.FirstOrDefault(d => d.Name == nextName)?.GetRelativePath(relativePath.Substring(idx + 1)) ?? throw new DirectoryNotFoundException();
        }
    }

    public sealed class FsLazyRepositoryRoot : FsLazyRepositoryDirectory
    {
        public FsLazyRepositoryRoot(DirectoryInfo di) : base(di, null)
        {
        }

        public override string Name
        {
            get => "Home";
            set => throw new NotSupportedException("Cannot change the name of a repository root");
        }

        public override string Path => "/";

        internal override void DeleteInternal()
        {
            throw new NotSupportedException("Cannot delete a repository root");
        }

        public override IRepositoryDirectory Parent => this;

        public override IRepositoryNode GetRelativePath(string relativePath)
        {
            return base.GetRelativePath(relativePath.StartsWith("/") ? relativePath.Substring(1) : relativePath);
        }
    }

    public sealed class FsLazyRepositoryFile : IRepositoryFile
    {
        private readonly FileInfo _fi;

        public FsLazyRepositoryFile(FileInfo fi, IRepositoryDirectory parent)
        {
            _fi = fi;
            Parent = parent;
            Name = fi.Name;
            SetRelativePath();
        }

        internal void DeleteInternal()
        {
            _fi.Delete();
        }

        private void SetRelativePath()
        {
            Path = Parent.Path + Name;
        }

        public string Name { get; set; }
        public string Path { get; private set; }
        public IRepositoryDirectory Parent { get; }
        public DateTime CreatedOn { get; private set; }

        public void SetContent(byte[] value) => File.WriteAllBytes(_fi.FullName, value);

        public byte[] GetContent() => File.ReadAllBytes(_fi.FullName);

        public long Size => new FileInfo(_fi.FullName).Length;
    }
}