using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace phimath.phileio.Data
{
    [Table("user")]
    public class User
    {
        [Key] [Column] public string Id { get; set; }
        public List<RepositoryUser> Repositories { get; set; }
        
        public override string ToString()
        {
            return Id;
        }
    }
}