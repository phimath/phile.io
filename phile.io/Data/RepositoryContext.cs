using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using phimath.phileio.Services;

namespace phimath.phileio.Data
{
    public sealed class RepositoryContext : DbContext
    {
        private readonly IUserAccessor _userAccessor;
        private DbSet<Repository> Repositories { get; set; }
        private DbSet<User> Users { get; set; }
        private DbSet<RepositoryUser> AccessRights { get; set; }
        private DbSet<Share> Shares { get; set; }

        public RepositoryContext(DbContextOptions options, IUserAccessor userAccessor) : base(options)
        {
            _userAccessor = userAccessor;
            Database.Migrate();
        }

        public bool HasRepositories()
        {
            return Repositories.Any();
        }

        public IEnumerable<Repository> GetRepositories()
        {
            return Repositories;
        }

        public IQueryable<User> GetOtherUsers()
        {
            return Users.Where(u => u.Id != _userAccessor.NameId);
        }

        public IQueryable<RepositoryUser> GetMyRepositories()
        {
            var result = AccessRights.Include(r => r.Repository).Where(r => r.UserId == _userAccessor.NameId);
            return result;
        }

        public Repository GetRepositoryAndCheckAccess(int id)
        {
            if (!_userAccessor.IsAuthenticated)
            {
                return null;
            }

            var result = AccessRights.Include(r => r.Repository).Single(r => r.RepositoryId == id && r.UserId == _userAccessor.NameId);

            if (result == null)
            {
                if (Repositories.Find(id) != null)
                {
                    throw new UnauthorizedAccessException();
                }

                throw new KeyNotFoundException();
            }

            return result.Repository;
        }

        public bool HasAccess(int repositoryId, string userId, out string role)
        {
            var result = AccessRights.Find(repositoryId, userId);
            if (result != null)
            {
                role = result.Role;
                return true;
            }

            role = null;
            return false;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Repository>()
                .HasMany(r => r.Users)
                .WithOne(ru => ru.Repository)
                .HasForeignKey(ru => ru.RepositoryId);
            modelBuilder.Entity<User>()
                .HasMany(u => u.Repositories)
                .WithOne(ru => ru.User)
                .HasForeignKey(ru => ru.UserId);
            modelBuilder.Entity<RepositoryUser>()
                .HasKey(ru => new {ru.RepositoryId, ru.UserId});
            modelBuilder.Entity<Share>()
                .HasOne(s => s.Repository)
                .WithMany(r => r.Shares)
                .HasForeignKey(s => s.RepositoryId);
        }

        public IEnumerable<Share> GetShares(int repositoryId, string path)
        {
            return Shares.Where(s => s.RepositoryId == repositoryId && s.RelativePath == path);
        }

        public async void EnsureUserCreated()
        {
            var nameId = _userAccessor.NameId;
            if (await Users.FindAsync(nameId) != null)
            {
                return;
            }

            await Users.AddAsync(new User {Id = nameId});
            await SaveChangesAsync();
        }
    }
}