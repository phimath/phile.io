using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace phimath.phileio.Data
{
    [Table("share")]
    public sealed class Share
    {
        [Key] [Column] public Guid Id { get; set; }
        [Column] public int RepositoryId { get; set; }
        public Repository Repository { get; set; }
        [Column] public string RelativePath { get; set; }
    }
}