using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace phimath.phileio.Data
{
    [Table("repository")]
    public class Repository
    {
        [Key] [Column] public int Id { get; set; }
        [Required] [Column] public string Name { get; set; }

        [InverseProperty(nameof(RepositoryUser.Repository))]
        public IList<RepositoryUser> Users { get; set; }

        [InverseProperty(nameof(Share.Repository))]
        public IList<Share> Shares { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}