using System.ComponentModel.DataAnnotations.Schema;

namespace phimath.phileio.Data
{
    [Table("repository_user")]
    public class RepositoryUser
    {
        public int RepositoryId { get; set; }
        public Repository Repository { get; set; }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Role { get; set; }
    }

    public static class Roles
    {
        public const string OWNER = "Owner";
        public const string MANAGER = "Manager";
        public const string MEMBER = "Member";
    }
}