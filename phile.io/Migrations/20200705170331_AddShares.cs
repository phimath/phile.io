﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace phimath.phileio.Migrations
{
    public partial class AddShares : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "share",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    RepositoryId = table.Column<int>(nullable: false),
                    RelativePath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_share", x => x.Id);
                    table.ForeignKey(
                        name: "FK_share_repository_RepositoryId",
                        column: x => x.RepositoryId,
                        principalTable: "repository",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_share_RepositoryId",
                table: "share",
                column: "RepositoryId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "share");
        }
    }
}
