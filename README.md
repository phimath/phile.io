# phile.io

phile.io is a simple file sharing server created with ASP.NET Core.

It uses a low-profile sqlite backend for storing permissions and handles login 
via SAML/OAuth2 or WS-Federation. The files are stored directly on your 
filesystem, and are thus easy to back up.

If you want to find out more, check out the [project website](https://www.phimath.de/phile-io/)!